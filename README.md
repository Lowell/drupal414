# Where we meet to learn-share-discover Drupal

A demo website for the Drupal414 Meetups
<br>We will demonstrate various skills and techniques over the coming months
<br>Will this become the official `www.drupal414.com` website?
<br>Visit the demo at http://drupal414.version30.com
<br>View the app here http://app.drupal414.version30.com/index.html

## In this repo (caution: this will get the full D8 codebase)
* `git clone git@gitlab.com:Lowell/drupal414.git --recursive`
* code for the drupal414 web/ios/android app
* drupal core and configuration files

## Links to individual repos
* `git clone git@gitlab.com:Lowell/drupal414---ionicApp.git`
* `git clone git@gitlab.com:Lowell/drupal414---deploy.git`

## At the next Meetup
#### December 17th, 2015
1. Drupal 8
2. add some content
3. expose that content as JSON.
4. create a simple front-end application
5. connect it to the Drupal414 website
6. convert this application into an Android app and an IOS app

__Notes and Links:__
* Javascript, Angular, and Ionic
* this is one possible workflow of probably thousands
* https://www.vagrantup.com/
* https://www.virtualbox.org/
* https://github.com/driftyco/ionic-box
* http://view.ionic.io/
* username: `drupal414@digitalfarrier.com`
* password: `drupal414`